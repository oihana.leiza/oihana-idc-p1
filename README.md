# **P1 -** **Validated** **Container** **Delivery**

 ![Made-with-Markdown](https://img.shields.io/badge/Made%20with-Markdown-success)

 [![pipeline status](https://gitlab.com/oihana.leiza/practica_ci_tdd/badges/master/pipeline.svg)](https://gitlab.com/oihana.leiza/practica_ci_tdd/commits/master)

Este repositorio contiene la **Práctica 1º** denominada **Validated Container Delivery** de la asignatura **Integración y Despliegue Continuo** del master [Análisis de datos, Ciberseguridad y Computación en la nube](https://www.mondragon.edu/es/master-universitario-analisis-datos-ciberseguridad-computacion-nube) 

La práctica tiene por objetivo ser capaz de **diseñar e implementar un pipeline CI** para el sistema de pedidos (“Orders”). 

## Changelog

Con el desarrollo de esta práctica se ha liberado una nueva release del proyecto Orders. A continuación, los principales cambios:

* Nuevo test suite de **pruebas unitarias** con el se proporciona una cobertura del 20%.
* Nuevo **Smoke Test Suite** para el proyecto.
* Nueva **funcionalidad** "nombre-de-la-funcionalidad".
* Se ha diseñado, implementado y desplegado el **pipeline CI**.
* Se ha desarrollado un script de comandos SSH para el **despliegue manual** en Stage de la release.
* Se ha gestionado el desarrollo en base a la **metodología ágil** descrita en `doc.md`

## Documentation

Los detalles de las especificaciones, diseño y evidencias del desarrollo de la práctica quedan recogidos en la documentación complementaria [doc.md](doc.md).

## Authors

Oihana Leiza Alzuart– [oihana.leiza@alumni.mondragon.edu](oihana.leiza@alumni.mondragon.edu)

##  License
Este proyecto usa la licencia [MIT License](https://choosealicense.com/licenses/mit/),  ver `LICENSE.md` para más detalles.