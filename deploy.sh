# Container registry
export CI_REGISTRY=registry.gitlab.com

# Clone the repository
git clone https://gitlab.com/oihana.leiza/oihana-idc-p1.git
cd oihana-idc-p1

# Deployment
docker login $CI_REGISTRY
docker-compose up -d