# P1-doc
En este documento se recogen la definición y evidencias de la **Práctica 1º** denominada **Validated Container Delivery** de la asignatura **Integración y Despliegue Continuo** del master [Análisis de datos, Ciberseguridad y Computación en la nube](https://www.mondragon.edu/es/master-universitario-analisis-datos-ciberseguridad-computacion-nube) 

## Test Unitarios

Se han realizado una serie de tests que comprueban el funcionamiento del sistema. Al tratarse de una aplicación sencilla se han realizado tests sencillos que llaman a la aplicación desde dentro (sin usar las rutas) y des de fuera (con la API, las rutas).

## Smoke Test Suite

Se han tomado tres funcionalidades principales del sistema, que se ha visto que es necesario que funcionen correctamente para que podamos decir que el software funciona correctamente.

- User: los pedidos tienen que se realizados por un usuario, por lo que es necesario que la creación de estos pueda ser realizada.

- Order: se trata de una aplicación para realizar pedidos, así que esta funcionalidad es también importante.

- Machine: los pedidos se realizan para fabricar piezas, por ello se comprueba que al hacer un pedido esas piezas son fabricadas.

![smoke-test](images/smoke.png)



## Nueva funcionalidad: gestión de usuarios

La nueva funcionalidad que se ha añadido es la de creación de usuarios. Para ello primero se han creado tests que comprueban que la creación de usuarios puede realizarse  mediante la ruta "/create_user". Después se ha creado un issue en el que se ha trabajado para poner esa funcionalidad en marcha.

## Pipeline CI

El pipeline se ha compuesto por tres fases diferentes:

* Build: se crea la imagen con docker
* Test: se testea el correcto funcionamiento de la aplicacion tanto el formato del código.
* Delivery: se suben contenedores al container registry.

En la siguiente imagen vemos una captura de los contenedores generados:

![containers](images/containers.png)


## Deploy manual

Se ha realizado un deployment manual utilizando AWS. Para ello primero se ha creado una instancia de ubuntu pública y se accede por SSH:

![instance](images/instancia.png)

Tras instalar docker y docker-compose se han seguido los pasos de deploy.sh para poder poner así la aplicación en marcha. De esta manera se puede acceder a la aplicación desde fuera:

![instance](images/awsusers.png)

## Gestión agile

La gestión del proyecto se ha llevado a cabo mediante el uso de milestones, con los que se ha dividido el proyecto en diferentes tareas generales.

![milestones](images/milestones.png)

### Board

Para facilitar la gestión de los diferentes issues del proyecto se ha utilizado un issue board sencillo donde tenemos cuatro columnas. En la primera vemos todos los issues abiertos. En la segunda tenemos la etiqueta ToDo, lo cual indica cuales son los issues a realizar en el futuro próximo, después de los issues de la columna Doing, ya que allí tenemos los issues en los que se trabaja en el momento. Por último tenemos la columna de closed issues, que nos sirve para visualizar cual ha sido el trabajo realizado hasta el momento.

![issueboard](images/issueboard.PNG)

### Issues

Los issues han sido la unidad de trabajo más pequeña del proyecto, tareas que en su conjunto completan milestones. Para poder asegurar que la rama principal siempre tiene un funcionamiento correcto, para cada issue se ha creado una rama donde se ha realizado el desarrollo requerido junto a tests que verifiquen un funcionamiento correcto.

![rama](images/rama.PNG)

 Una vez se sepa que los tests pasan correctamente se sube a gitlab. Si el pipeline pasa correctamente los cambios son añadidos a la rama principal, master.

![pipeline](images/pipeline.PNG)

Como podemos ver en la imagen, si el resultado del pipeline no es satisfactorio se vuelven a hacer cambios en la misma rama, y se ejecuta el pipeline nuevamente. Si los cambios han hecho que el pipeline sa correcto por último se puede proceder a hacer un merge a master.