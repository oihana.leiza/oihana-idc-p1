import pytest

from application import Session, create_app, engine


@pytest.fixture
def supply_url():
    # return "http://10.100.199.200:8000"
    return "http://localhost:8000"


@pytest.fixture(scope="function")
def test_app():
    app = create_app()
    client = app.test_client()
    client.testing = True
    ctx = app.test_request_context()
    ctx.push()

    yield client

    Session.close()
    engine.dispose()

    pass
