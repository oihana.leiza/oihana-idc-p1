import json

import requests


def test_smoke_create_order(supply_url):

    ## Create an order and check correct status
    url = supply_url + "/order"
    data = {
        "description": "New order created from REST API",
        "number_of_pieces": 7,
        "user_id": 1,
    }

    resp = requests.post(
        url, data=json.dumps(data), headers={"Content-type": "application/json"}
    )

    print(resp)

    data = resp.json()
    order_id = data["id"]
    assert resp.status_code == 200
    assert data["status"] == "Created"

    # Check number of manufactured pieces it's okay
    url = supply_url + "/order/" + str(order_id)
    resp = requests.get(url, headers={"Content-type": "application/json"})
    data = resp.json()
    assert resp.status_code == 200
    assert data["number_of_pieces"] == 7
