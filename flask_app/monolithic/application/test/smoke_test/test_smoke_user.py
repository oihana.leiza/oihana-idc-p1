import json

import requests


def test_smoke_create_user(supply_url):

    ## Create a user
    url = supply_url + "/create_user"
    data = {"name": "Joxepe", "password": "pasword", "postalCode": "01234"}

    resp = requests.post(
        url, data=json.dumps(data), headers={"Content-type": "application/json"}
    )

    print(resp)

    data = resp.json()
    user_id = data["id"]
    assert resp.status_code == 200

    # Check user exists on the system
    url = supply_url + "/user/" + str(user_id)
    resp = requests.get(url, headers={"Content-type": "application/json"})
    data = resp.json()
    assert resp.status_code == 200
