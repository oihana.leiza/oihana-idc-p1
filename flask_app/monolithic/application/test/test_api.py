import json

import requests

from application import Session
from application.models import Order, User


def test_new_user():
    session = Session()
    new_user = User(name="Joxemari", password="pasahitza", postalCode="01123")
    session.add(new_user)
    session.commit()
    user = session.query(User).get(new_user.id)
    assert user.id == new_user.id, "Error al insertar nuevo usuario"
    session.close()


def test_new_order():
    session = Session()
    new_order = Order(
        description="Test order",
        number_of_pieces=5,
        status=Order.STATUS_CREATED,
        user_id=1,
    )
    session.add(new_order)
    session.commit()
    order = session.query(Order).get(new_order.id)
    assert order.id == new_order.id, "Error al insertar una orden"
    session.close()


def test_list_orders(supply_url):
    url = supply_url + "/orders"
    resp = requests.get(url)
    assert resp.status_code == 200


def test_list_users(supply_url):
    url = supply_url + "/users"
    resp = requests.get(url)
    assert resp.status_code == 200
